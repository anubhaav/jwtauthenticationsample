﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;


namespace JwtAuthenticationSample.Controllers
{
    [Authorize(AuthenticationSchemes =
    JwtBearerDefaults.AuthenticationScheme)]
    [Route("Api/[controller]")]
    [ApiController]
    public class SafeController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
    
        public SafeController(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        [Route("GetUser")]
        public async Task<IActionResult> GetMessageAsync()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                return Ok(user);
                
            }
            return BadRequest("Error");
        }

        #region Helpers
        private async Task<IdentityUser> GetCurrentUserAsync()
        {
            string userName = HttpContext.User.Identity.Name;
            return await _userManager.FindByNameAsync(userName);
        }
        #endregion
    }
}
